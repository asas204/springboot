package com.tutorial.springboot.config;

import com.tutorial.springboot.filter.SitemeshFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by asas2.
 * FileName : ServletFilterConfig
 * Date: 2019-12-11
 * Time: 오전 11:12
 */
@Configuration
public class ServletFilterConfig {

    // sitemesh 설정부분
    @Bean
    public FilterRegistrationBean siteMeshFilter() {
        FilterRegistrationBean filter = new FilterRegistrationBean();
        filter.setFilter(new SitemeshFilter()); //2번에서 만든 클래스 이름으로 사용
        return filter;
    }

}