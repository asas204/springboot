package com.tutorial.springboot.util.framework;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Created by asas2.
 * FileName : PasswordEncoding
 * Date: 2019-12-11
 * Time: 오전 9:50
 */
public class PasswordEncoding implements PasswordEncoder {

    private PasswordEncoder passwordEncoder;

    public PasswordEncoding() {
        this.passwordEncoder = new BCryptPasswordEncoder();
    }

    public PasswordEncoding(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public String encode(CharSequence charSequence) {
        return passwordEncoder.encode(charSequence);
    }

    @Override
    public boolean matches(CharSequence charSequence, String password) {
        return passwordEncoder.matches(charSequence, password);
    }
}
