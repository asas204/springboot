package com.tutorial.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created by asas2.
 * FileName : HelloController
 * Date: 2019-12-06
 * Time: 오후 4:26
 */
@Controller
public class HelloController {

    @RequestMapping(value = "/hello.do", method = {RequestMethod.GET, RequestMethod.POST})
    public String greet(@RequestParam Map<String, String> paramMap){

        System.out.println("test");

        return "index";
    }
}
