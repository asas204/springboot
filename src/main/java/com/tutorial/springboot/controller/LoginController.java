package com.tutorial.springboot.controller;

import com.tutorial.springboot.service.LoginService;
import com.tutorial.springboot.util.framework.PasswordEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by asas2.
 * FileName : LoginController
 * Date: 2019-12-11
 * Time: 오전 9:23
 */
@Controller
public class LoginController {

    private Logger logger = LoggerFactory.getLogger("springboot.log");

    @Autowired
    LoginService loginService;

    @RequestMapping(value = "/login/loginform.do", method = {RequestMethod.GET})
    public String moveLoginForm(@RequestParam Map<String, String> paramMap, HttpServletRequest request) {
        return "login/loginForm";
    }

    @RequestMapping(value = "/login/login.ajax", method = {RequestMethod.POST}, produces = "application/json; charset=UTF-8")
    @ResponseBody
    public Map<String, Object> login(@RequestParam Map<String, String> paramMap, HttpServletRequest request, HttpServletResponse response) {
        Map<String, Object> result = new HashMap<>();
        try {
            String userId = paramMap.get("userId");
            String userPwd = paramMap.get("userPwd");

            // password encrypt
            PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            PasswordEncoding passwordEncoding = new PasswordEncoding(passwordEncoder);

            Map<String, String> userinfo = loginService.selectLoginInfo(paramMap);

            if (userPwd != null && userinfo.get("userPwd") != null) {
                if (passwordEncoding.matches(userPwd, userinfo.get("userPwd").toString())) {
                    // 비밀번호 일치시
                    result.put("result", true);
                } else {
                    // 비밀번호 불일치시
                    result.put("result", false);

                    // 비밀번호 틀린 횟수 확인
                    int pwdFailCnt = Integer.parseInt(loginService.retrievePwdFailCnt(paramMap).get("pwdFailCnt"));
                    if (pwdFailCnt > 5) {
                        // 5회 이상 틀렸을 경우
                        result.put("msg", "비밀번호를 5회 연속으로 틀려 계정이 잠겼습니다. 다시 사용하시려면 고객센터로 문의해주세요.");
                    } else {
                        loginService.updatePwdFailCnt(paramMap);
                        result.put("msg", "로그인 계정을 확인할 수 없습니다. 아이디(이메일)/비밀번호를 다시 확인해주세요.");
                    }
                    logger.info("login Fail" + "id : " + userId + "rsn : " + "User Not Exist.");
                }

            } else {
                // DB 혹은 입력받은 비밀번호가 null일 경우
                result.put("result", false);
                result.put("msg", "로그인 계정을 확인할 수 없습니다. 아이디(이메일)/비밀번호를 다시 확인해주세요. ");
                logger.info("login Fail" + "id : " + userId + "rsn : " + "User Not Exist.");
            }
        } catch (UsernameNotFoundException e) {
            result.put("result", false);
            result.put("msg", "로그인 계정을 확인할 수 없습니다. 아이디(이메일)/비밀번호를 다시 확인해주세요. ");
            logger.info("login Fail" + "id : " + paramMap.get("userId").toString() + "rsn : " + "Error.");
        } catch (Exception e) {
            result.put("result", false);
            result.put("msg", "로그인에 실패하였습니다.");
            logger.info("login Fail" + "id : " + paramMap.get("userId").toString() + " rsn : " + "Error.");
        }

        return result;
    }
}
