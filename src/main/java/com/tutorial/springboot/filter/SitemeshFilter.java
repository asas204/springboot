package com.tutorial.springboot.filter;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;

/**
 * Created by asas2.
 * FileName : SitemeshFilter
 * Date: 2019-12-11
 * Time: 오전 11:13
 */
public class SitemeshFilter extends ConfigurableSiteMeshFilter {

    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
        builder
                .addDecoratorPath("/login/*.do", "/WEB-INF/jsp/decorator/loginLayout.jsp")
                .addDecoratorPath("/*.do", "/WEB-INF/jsp/decorator/bodyLayout.jsp")
                .addDecoratorPath("/*.pop", "/WEB-INF/jsp/decorator/popupLayout.jsp");
        // .addExcludedPath("/login/*"); // 데코레이션 제외 경로
    }

}
