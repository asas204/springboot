package com.tutorial.springboot.service.mapper;

import com.tutorial.springboot.dto.UserDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * Created by asas2.
 * FileName : LoginMapper
 * Date: 2019-12-10
 * Time: 오후 5:00
 */
@Mapper
public interface LoginMapper{
    Map<String, String> selectLoginInfo(Map<String, String> paramMap);

    UserDto selectLoginInfoSecurity(Map<String, String> paramMap);

    int updatePwdFailCnt(Map<String, String> paramMap);

    Map<String, String> retrievePwdFailCnt(Map<String, String> paramMap);

    int createUser(Map<String, String> paramMap);
}
