package com.tutorial.springboot.service.impl;

import com.tutorial.springboot.dto.UserDto;
import com.tutorial.springboot.service.LoginService;
import com.tutorial.springboot.service.mapper.LoginMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by asas2.
 * FileName : LoginServiceImpl
 * Date: 2019-12-10
 * Time: 오후 5:00
 */
@Service(value="loginservice")
public class LoginServiceImpl implements LoginService {

    @Autowired
    LoginMapper loginMapper;


    @Override
    public Map<String, String> selectLoginInfo(Map<String, String> paramMap) {
        Map<String, String> userinfo = loginMapper.selectLoginInfo(paramMap);
        return userinfo;
    }

    // security 로그인 정보 값
    @Override
    public UserDto selectLoginInfoSecurity(Map<String, String> paramMap) {
        UserDto userinfo = loginMapper.selectLoginInfoSecurity(paramMap);
        return userinfo;
    }

    @Override
    public int updatePwdFailCnt(Map<String, String> paramMap) {
        return loginMapper.updatePwdFailCnt(paramMap);
    }

    @Override
    public Map<String, String> retrievePwdFailCnt(Map<String, String> paramMap) {
        return loginMapper.retrievePwdFailCnt(paramMap);
    }

    @Override
    public int createUser(Map<String, String> paramMap) {
        return loginMapper.createUser(paramMap);
    }
}
