package com.tutorial.springboot.service;

import com.tutorial.springboot.dto.UserDto;

import java.util.Map;

/**
 * Created by asas2.
 * FileName : Loginservice
 * Date: 2019-12-10
 * Time: 오후 5:00
 */
public interface LoginService {
    Map<String, String> selectLoginInfo(Map<String, String> paramMap);

    UserDto selectLoginInfoSecurity(Map<String, String> paramMap);

    int updatePwdFailCnt(Map<String, String> paramMap);

    Map<String, String> retrievePwdFailCnt(Map<String, String> paramMap);

    int createUser(Map<String, String> paramMap);
}
