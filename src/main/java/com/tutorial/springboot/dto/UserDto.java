package com.tutorial.springboot.dto;

/**
 * Created by asas2.
 * FileName : UserDto
 * Date: 2019-12-09
 * Time: 오전 11:17
 */
public class UserDto {

    private String companyId        = "";   // 회사 ID (UUID)
    private String userId           = "";   // 사용자 ID (UUID)
    private String userPwd          = "";   // 사용자 비밀번호
    private String userNm           = "";   // 사용자 이름
    private String email            = "";   // 사용자 이메일
    private String celPhone         = "";   // 사용자 핸드폰 번호

    UserDto () {

    }

    public UserDto(String companyId, String userId, String userPwd, String userNm, String email, String celPhone) {
        this.companyId = companyId;
        this.userId = userId;
        this.userPwd = userPwd;
        this.userNm = userNm;
        this.email = email;
        this.celPhone = celPhone;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserNm() {
        return userNm;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCelPhone() {
        return celPhone;
    }

    public void setCelPhone(String celPhone) {
        this.celPhone = celPhone;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "companyId='" + companyId + '\'' +
                ", userId='" + userId + '\'' +
                ", userPwd='" + userPwd + '\'' +
                ", userNm='" + userNm + '\'' +
                ", email='" + email + '\'' +
                ", celPhone='" + celPhone + '\'' +
                '}';
    }
}
