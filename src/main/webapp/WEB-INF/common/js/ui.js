var transitionend = 'transitionend webkitTransitionEnd oTransitionEnd otransitionend';

$(document).ready(function() {

	// Sidebar gnb
	$(document).on('click','.gnb a.has-expand',function(e){
		e.preventDefault();
		if ($(this).parent().hasClass('is-active')){
			$(this).parent().removeClass('is-active');
			$(this).next().stop().slideUp('fast');
		}else{
			$(this).parent().siblings().find('>a').next().slideUp('fast');
			$(this).parent().siblings().removeClass('is-active');
			$(this).parent().addClass('is-active');
			$(this).next().stop().slideDown('fast');
		}
		$(this).find('.ico-gnb-fold.dep1')
	});


	// Accordion
	$(document).on('click','.status-list .item dl > dt',function(e){
		e.preventDefault();
		if ($(this).closest('.item').hasClass('is-active')){
			$(this).closest('.item').removeClass('is-active');
			$(this).next().stop().slideUp('fast');
		}else{
			$(this).closest('.item').siblings().find('dd').slideUp('fast');
			$(this).closest('.item').siblings().removeClass('is-active');
			$(this).closest('.item').addClass('is-active');
			$(this).next().stop().slideDown('fast');
		}
	});


	// Datepicker
	$('.datepicker').each(function(){
		var id = '#'+$(this).parent().attr('id'); //classic 버전일경우 id로 찾아야함
		var options = {
			selectYears: true,
			selectMonths: true,
			format: 'yyyy/mm/dd',
			formatSubmit: 'yyyy/mm/dd',
			// min: [2015, 7, 14],
			container: id,
			// editable: true,
			closeOnSelect: false,
			closeOnClear: false,
		};
		var $input = $(this).pickadate(options);
		var picker = $input.pickadate('picker');
	});
	

	// Selectbox custom
	$('.select-dropdown dt').on('click', function() {
		$(this).closest('.select-wrap').find('.select-dropdown dd ul').slideToggle('fast');
	});

	$('.mutli-select ul li a').click(function() {
		var text = $(this).html();
		$(this).closest('.select-dropdown').find('.select-name').html(text);
		$('.select-dropdown dd ul').hide();
	});

	$(document).bind('click', function(e) {
		var $clicked = $(e.target);
		if (!$clicked.parents().hasClass('select-dropdown'))
			$(".select-dropdown dd ul").hide();
	});

	function getSelectedValue(id) {
		return $("#" + id).find("dt a span.value").html();
	}
	
	$('.mutli-select input[type="checkbox"]').on('click', function() {
		var title = $(this).closest('.mutli-select').find('input[type="checkbox"]').val(),
			title = $(this).val() + ",";
	
		if ($(this).is(':checked')) {
			var html = '<span title="' + title + '">' + title + '</span>';
			$('.select-value').append(html);
			$(".check-name").hide();
		} else {
			$('span[title="' + title + '"]').remove();
			var ret = $(".check-name");
			$('.select-dropdown dt a').append(ret);
		}
	});


	// Table sorter btn
	$(document).on('click','.tbl-wrap .tbl .btn-tableSorter',function(e){
		e.preventDefault();
		if ($(this).parent().hasClass('is-active')){
			$(this).parent().removeClass('is-active');
			$(this).addClass('is-current')
		}else{
			$(this).parent().siblings().find('button').removeClass('is-current');
			$(this).parent().siblings().removeClass('is-active');
			$(this).parent().addClass('is-active');
		}
	});

	
	// Swiper slide
	var mdcSlide = new Swiper('.mdcSlide-wrap .swiper-container', {
		slidesPerView: 3,
		spaceBetween: 40,
		speed: 500,
		resistanceRatio: 0, //좌우 움직임 저항 조정
		scrollbar: {
		  el: '.mdcSlide-wrap .swiper-scrollbar',
		  hide: true,
		},
		mousewheel: {
			invert: false,
		},
		navigation: {
			nextEl: '.mdcSlide-wrap .swiper-button-next',
			prevEl: '.mdcSlide-wrap .swiper-button-prev',
		},
	});
	

});


// mCustom scrollbar
$(window).on('load',function(){
	$(".mScroll").mCustomScrollbar({
		theme:'dark',
		mouseWheelPixels : 600, // 마우스휠 속도
		scrollInertia : 400 // 부드러운 스크롤 효과 적용
	});
	$(".mScroll-x").mCustomScrollbar({
		axis:'x',
		theme:'dark',
		mouseWheelPixels : 600, 
		scrollInertia : 400, 
		setWidth: 'auto',
		advanced:{ 
			autoExpandHorizontalScroll: true,
			updateOnContentResize: true,
			updateOnSelectorChange: true
		}
	});
});


/* Utility Functions */
// Dim layer
function funcDimOpen() {
	var $dim = $('.dim-mask');
	//$dim.addClass('is-visible');
	setTimeout(function(){ $dim.addClass('is-active') }, 50);
}
function funcDimClose() {
	var $dim = $('.dim-mask');
	//$dim.removeClass('is-visible');
	setTimeout(function(){ $dim.removeClass('is-active') }, 200);
}

// Loading
function loading (action, callback){
	var $eleModule = $('.loading-wrap');
	if (action == 'open'){
		$eleModule.removeAttr('hidden');
		setTimeout(function(){ $eleModule.addClass('is-active'); });
		$eleModule.one(transitionend, function(){
			if ($eleModule.hasClass('is-active')){
				if (callback){ callback }
			}
		})
		funcDimOpen();
	}
	else if (action == 'close'){
		$eleModule.removeClass('is-active');
		$eleModule.one(transitionend, function(){
			if (!$(this).hasClass('is-active')){
				$eleModule.attr('hidden', 'hidden');
				if (callback){ callback }
			}
		});
		funcDimClose();
	}
}

/*
	gnbSet
	호출함수: ui.gnbSet(n1, n2, n3)
	호출예시: ui.sidebar(1, 2, 3)
*/


// Sidebar gnbset
function gnbSet (n1, n2){
	var $gnb = $('.gnb');
	var $n1, $n2;

	if (typeof(n1) == 'number'){
		$n1 = $gnb.find('.node-menu1 > ul > li').eq(n1);
		$n1.addClass('is-current is-active');
	}
	if (typeof(n2) == 'number'){
		$n2 = $n1.find('.node-menu2 > ul > li').eq(n2);
		$n2.addClass('is-current is-active');
	}
	setTimeout(function(){ $gnb.addClass('is-load') }, 350);
}

// Modal popup
function openModalPopup(btnObj, modalObj){	
	var targetFrame = $(modalObj);
	targetFrameIn = targetFrame.find('iframe'); // custom id 입력
	
	$('body').addClass('scrollOff');
	//$(modalObj).stop().fadeIn('fast')
	$(modalObj).stop().fadeIn('fast').attr('tabindex','0').find('.modalPopup').addClass('is-active');
	funcDimOpen();
}

function closeModalPopup(btnObj, modalObj){	
	var targetFrame = $(modalObj);
	targetFrameIn = targetFrame.find('iframe'); // custom id 입력
	
	$('body').removeClass('scrollOff');
	$(modalObj).stop().fadeOut('fast');
	funcDimClose();	
}

$(document).on('click','.popupClose', function(e){
	e.preventDefault();
	$(this).closest('.modalPopup').fadeOut('fast',function(){		
		$(this).closest(".modalPopup").hide();	
		$('body').removeClass('scrollOff');
	});
	funcDimClose();
});


// Input - file : 파일명 하단 리스트 추가 타입
function fileChange(obj){
    if (window.FileReader) { // modern browser
        var filename = $(obj)[0].files[0].name;
    } else { // old IE
        var filename = $(obj).val().split('/').pop().split('\\').pop(); // 파일명만 추출
    }
    // 추출한 파일명 삽입			
    $(obj).siblings('.upload-name').val(filename);
}


// mCustomScrollbar
// function mScrollUpdate(){
// 	$('.mScroll-x').each(function(){
// 		$(this).mCustomScrollbar("update");
// 		console.log('mScroll Update');
// 	})
// }
