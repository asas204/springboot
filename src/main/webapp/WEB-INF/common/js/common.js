//**********************************************************************************
// n분전까지 구하는 함수
//**********************************************************************************
function beforeTenMinutes() {
    var minutes = 10; // 9분전
    // @추가주석 : 데이터 때문에 날짜 값을 입력함.
    // 실제 데이터가 계속 1분마다 들어온다면, 해당 날짜를 제거
    // 만약 어느 특정 날짜로 시연할 경우 해당 날짜를 아래와 같이 입력
    var tDate = new Date('2019-11-04 03:30'); // todo : 하드코딩 제거
    var minutesArray = new Array();

    // now ~ 9분전 List
    for (var i = 0; i < 10; i++) {
        var hours = tDate.getHours();
        var min = tDate.getMinutes() - minutes;

        if (hours < 10) {
            hours = "0" + hours;
        }

        if (min < 10) {
            min = "0" + min;
        }

        minutesArray[i] = hours + ":" + min;
        minutes--;
    }

    console.log("minutesArray", minutesArray);

    return minutesArray;
}

//**********************************************************************************
// 오늘 날짜 구하는 함수
//**********************************************************************************
function todayDate() {
    // @추가주석 : 데이터 때문에 날짜 값을 입력함.
    // 실제 데이터가 계속 1분마다 들어온다면, 해당 날짜를 제거
    // 만약 어느 특정 날짜로 시연할 경우 해당 날짜를 아래와 같이 입력
    var tDate = new Date('2019-11-04 03:30'); // todo : 하드코딩 제거
    var year = tDate.getFullYear();
    var month = tDate.getMonth();
    var day = tDate.getDate();

    return year + "-" + ("0"+(month + 1)).slice(-2) + "-" + ("0"+(day)).slice(-2);
}


//**********************************************************************************
// Ajax call 이후 화면 전환 함수
//**********************************************************************************
function fnSetListData(id, data) {
    var divId = '#' + id;
    $(divId).html(data);
}