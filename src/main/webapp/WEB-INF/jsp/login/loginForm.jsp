<%--
  Created by IntelliJ IDEA.
  User: asas2
  Date: 2019-12-10
  Time: 오전 9:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>login</title>
    <script>

        //**********************************************************************************
        // 로그인 함수
        //**********************************************************************************
        function login() {

            if ($('#userId').val() == '') {
                alert('아이디(이메일)를 입력해주세요.');
                return;
            }
            if ($('#userPwd').val() == '') {
                alert('비밀번호를 입력해주세요.');
                return;
            }

            $.ajax({
                type : "POST",
                url : "/login/login.ajax",
                data : $("#loginForm").serialize(),
                beforeSend : function(xhr){
                    xhr.setRequestHeader("X-Ajax-call", "true");
                },
                success : function(data) {
                    console.log(data);
                    if (data.result) {
                        loading('open'); // 로딩실행
                        // login success
                        $('#loginForm').attr("action","/login/authenticate").attr("method", "post").submit();
                    } else {
                        // login fail
                        alert(data.msg);
                    }
                },
                error: function (xhr, status, error) {
                    console.log("code:"+xhr.status+"\n"+"message:"+xhr.responseText+"\n"+"error:"+error);
                }
            });
        }

        //todo test용 계정생성
        function createUser() {
            $.ajax({
                type : "POST",
                url : "/test/createuser.ajax",
                data : $("#createUserForm").serialize(),
                beforeSend : function(xhr){
                    xhr.setRequestHeader("X-Ajax-call", "true");
                },
                success : function(data) {
                    alert(data.msg);
                    location.reload();
                },
                error: function (xhr, status, error) {
                    console.log("code:"+xhr.status+"\n"+"message:"+xhr.responseText+"\n"+"error:"+error);
                }
            });
        }
    </script>
</head>
<body>
<div class="wrapper">
    <!-- s: container -->
    <div class="container">
        <!-- s: login page -->
        <div class="login-page">
            <div class="login-wrap">
                <div class="grid">
                    <div class="img-sec">

                    <div class="login-sec">
                        <div class="login-header">
                            <p class="login-logo">Data Center Infra<br>
                                Management<br>
                                System</p>
                        </div>
                        <div class="login-content">
                            <p class="txt">System Login</p>
                            <form id="loginForm">
                                <div class="form-wrap">
                                    <input type="text" name="userId" id="userId" placeholder="이메일 입력" title="" class="form-input">
                                    <input type="password" name="userPwd" id="userPwd" placeholder="비밀번호 입력" title="" class="form-input">
                                </div>
                                <div class="btn-wrap">
                                    <button onclick="login();return false;" class="btn btn-login"><span>LOGIN</span></button>
                                    <a href="#" class="btn btn-pw"><i class="ico ico-lock"></i><span>Forgot your password?</span></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- e: login page -->
    </div>
    <!-- e: container -->

    <!-- s: loading -->
    <div class="loading-wrap">
        <div id="loading" class="loading">
            <div class="loading-cont">
                <div class="box">
                    <div class="loader"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- e: loading -->

    <!-- dim mask -->
    <div class="dim-mask"></div>
</div>
<%--<h4>LOGIN ********</h4>
<form id="loginForm">
    <meta id="_csrf" name="_csrf" content="${_csrf.token}" />
    <meta id="_csrf_header" name="_csrf_header" content="${_csrf.headerName}" />
    <input type="text" id="userId" name="userId" placeholder="ID"/>
    <input type="password" id="userPwd" name="userPwd" placeholder="Password"/>
    <input type="button" value="Enter" onclick="login();return false;"/>
</form>--%>

<%--<h4>TEST USER CREATE ********</h4>
<form id="createUserForm">
    <input type="text" name="userId" placeholder="userId"/>
    <input type="password" name="userPwd" placeholder="userPwd"/>
    <input type="text" name="userNm" placeholder="userNm">
    <input type="text" name="userEmail" placeholder="userEmail">
    <input type="button" value="Create" onclick="createUser();return false;"/>
</form>--%>
</body>
</html>

