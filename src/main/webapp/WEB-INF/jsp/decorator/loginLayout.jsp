<%--
  Created by IntelliJ IDEA.
  User: Jeongwon
  Date: 2019-10-17
  Time: 오후 1:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title><sitemesh:write property='title' /></title>
    <!-- script -->
    <%--<script type="text/javascript" src="<c:url value="/common/js/libs/export-data.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/exporting.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/highcharts.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/jquery-1.12.4.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/jquery-3.3.1.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/jquery-ui.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/jquery.mCustomScrollbar.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/legacy.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/picker.date.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/picker.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/libs/series-label.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/ui.js"/>"></script>--%>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


    <!-- favicon -->
    <link href="/images/common/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="/images/common/favicon.ico" rel="icon" type="image/x-icon">
    <script src="/common/js/ui.js"></script>
    <sitemesh:write property='head' />
</head>
<body class="login">
<sitemesh:write property='body' />
</body>
</html>