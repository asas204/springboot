<%--
  Created by IntelliJ IDEA.
  User: Jeongwon
  Date: 2019-10-17
  Time: 오후 1:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta name="generator" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <title><sitemesh:write property='title' /></title>
    <%--<script type="text/javascript" src="<c:url value="/common/js/jquery11.1.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/common/js/jquery.form.js"/>"></script>--%>
    <sitemesh:write property='head' />
</head>
<body>
<div>Popup Layout</div>
<sitemesh:write property='body' />
</body>
</html>