<%--
  Created by IntelliJ IDEA.
  User: Jeongwon
  Date: 2019-10-17
  Time: 오후 1:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><!doctype html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title><sitemesh:write property='title' /></title>
    <!-- css -->
    <%--<link rel="stylesheet" href="<c:url value="/common/css/base.css"/>">
    <link rel="stylesheet" href="<c:url value="/common/css/common.css"/>">
    <link rel="stylesheet" href="<c:url value="/common/css/ui_style.css"/>">
    <link rel="stylesheet" href="<c:url value="/common/css/layout.css"/>">
    <link rel="stylesheet" href="<c:url value="/common/css/content.css"/>">
    <link rel="stylesheet" href="<c:url value="/common/css/libs/animate.css"/>">
    <link rel="stylesheet" href="<c:url value="/common/css/libs/swiper.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/common/css/libs/picker.classic.css"/>">
    <link rel="stylesheet" href="<c:url value="/common/css/libs/picker.classic.date.css"/>">
    <link rel="stylesheet" href="<c:url value="/common/css/libs/jquery.mCustomScrollbar.min.css"/>">
    <!-- script -->
    <script type="text/javascript" src="<c:url value="/common/js/jquery11.1.min.js"/>"></script>
    <script src="<c:url value="/common/js/libs/jquery-3.3.1.min.js"/>"></script>
    <script src="<c:url value="/common/js/libs/swiper.min.js"/>"></script>
    <script src="<c:url value="/common/js/libs/picker.js"/>"></script>
    <script src="<c:url value="/common/js/libs/picker.date.js"/>"></script>
    <script src="<c:url value="/common/js/libs/legacy.js"/>"></script>
    <script src="<c:url value="/common/js/libs/jquery.mCustomScrollbar.min.js"/>"></script>
    <script src="<c:url value="/common/js/libs/highcharts.js"/>"></script>
    <script src="<c:url value="/common/js/libs/series-label.js"/>"></script>
    <script src="<c:url value="/common/js/libs/exporting.js"/>"></script>
    <script src="<c:url value="/common/js/libs/export-data.js"/>"></script>
    <script src="<c:url value="/common/js/ui.js"/>"></script>
    <script src="<c:url value="/common/js/common.js"/>"></script>
    <script src="<c:url value="/common/js/jquery.form.js"/>"></script>--%>
    <!-- favicon -->
    <link href="/images/common/favicon.ico" rel="shortcut icon" type="image/x-icon">
    <link href="/images/common/favicon.ico" rel="icon" type="image/x-icon">

    <sitemesh:write property='head' />
</head>

<body>
<div class="wrapper over-flow">

    <!-- s: Sidebar -->
    <aside id="sidebar" class="sidebar">
        <!-- s: sidebar-header -->
        <div class="sidebar-header">
            <!-- <p class="logo"><a href="/"><img src="../../images/common/logo.png" alt="LOGO"></a></p> -->
            <p class="sidebar-txt">Data Center Infra<br>Management<br>System</p>
        </div>
        <!-- e: sidebar-header -->

        <!-- s: sidebar-container -->
        <div class="sidebar-container">
            <!-- s: GNB -->
            <div class="gnb-wrap">
                <nav class="gnb">
                    <div class="node-menu1">
                        <ul>
                            <li>
                                <a href="/monitoring/monitoringform.do">
                                    <i class="ico ico-gnb menu1"></i><span class="node-label1">모니터링</span><i class="ico ico-gnb-fold dep1"></i>
                                </a>
                            </li>
                            <li>
                                <a href="/device/retrievedeviceform.do">
                                    <i class="ico ico-gnb menu2"></i><span class="node-label1">Device 관리</span><i class="ico ico-gnb-fold dep1"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="has-expand">
                                    <i class="ico ico-gnb menu3"></i><span class="node-label1">원격진단/자가복구 관리</span><i class="ico ico-gnb-fold dep1"></i>
                                </a>
                                <div class="node-menu2">
                                    <ul>
                                        <li><a href="/diagnosis/remote/diagnosislistform.do">원격진단 설정</a></li>
                                        <li><a href="/diagnosis/healing/healinglistform.do">자가복구 설정</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="ico ico-gnb menu4"></i><span class="node-label1">정책 관리</span><i class="ico ico-gnb-fold dep1"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="ico ico-gnb menu5"></i><span class="node-label1">알림 Noti 설정</span><i class="ico ico-gnb-fold dep1"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="has-expand">
                                    <i class="ico ico-gnb menu6"></i><span class="node-label1">이력 관리</span><i class="ico ico-gnb-fold dep1"></i>
                                </a>
                                <div class="node-menu2">
                                    <ul>
                                        <li><a href="/history/historymgmtform.do">원격진단 이력</a></li>
                                        <li><a href="/history/healinghistorymgmtform.do">자가복구 이력</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="javascript:;" class="has-expand">
                                    <i class="ico ico-gnb menu7"></i><span class="node-label1">통계 관리</span><i class="ico ico-gnb-fold dep1"></i>
                                </a>
                                <div class="node-menu2">
                                    <ul>
                                        <li><a href="javascript:;">이력 통계관리</a></li>
                                        <li><a href="javascript:;">자가복구 통계관리</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="ico ico-gnb menu8"></i><span class="node-label1">과금 관리</span><i class="ico ico-gnb-fold dep1"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="ico ico-gnb menu9"></i><span class="node-label1">사용자/권한 관리</span><i class="ico ico-gnb-fold dep1"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- e: GNB -->
        </div>
        <!-- e: sidebar-container -->
    </aside>
    <!-- e: Sidebar -->

    <!-- s: container -->
    <div class="container">
        <!-- s: content -->
        <sitemesh:write property='body' />
        <!-- e: content -->
    </div>
    <!-- e: container -->
</div>
</body>
</html>